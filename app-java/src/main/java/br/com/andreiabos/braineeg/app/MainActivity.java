package br.com.andreiabos.braineeg.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import br.com.andreiabos.braineeg.app.lib.WebServiceUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.github.pwittchen.neurosky.library.NeuroSky;
import com.github.pwittchen.neurosky.library.exception.BluetoothNotEnabledException;
import com.github.pwittchen.neurosky.library.listener.ExtendedDeviceMessageListener;
import com.github.pwittchen.neurosky.library.message.enums.BrainWave;
import com.github.pwittchen.neurosky.library.message.enums.Signal;
import com.github.pwittchen.neurosky.library.message.enums.State;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
  Tela para conexão do sensor

 TODO Adicionar os valores das leituras do NeuroSky ao gráfico da GetCredentialsActivity.java.
 */
public class MainActivity extends AppCompatActivity {

  private final static String LOG_TAG = "NeuroSky";
  public static NeuroSky neuroSky;
  public static boolean graphicsAreReady;

  private ArrayList<String> dataLog = new ArrayList();


  @BindView(R.id.tv_state) TextView tvState;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_connect);

    ButterKnife.bind(this);

    neuroSky = createNeuroSky();
    graphicsAreReady = false;
  }

  @Override protected void onResume() {
    super.onResume();
    if (neuroSky != null && neuroSky.isConnected()) {
      neuroSky.start();
    }
  }

  @Override protected void onPause() {
    super.onPause();
    if (neuroSky != null && neuroSky.isConnected()) {
      neuroSky.stop();
    }
  }

  @NonNull private NeuroSky createNeuroSky() {
    return new NeuroSky(new ExtendedDeviceMessageListener() {
      @Override public void onStateChange(State state) {
        handleStateChange(state);
      }

      @Override public void onSignalChange(Signal signal) {
        handleSignalChange(signal);
      }

      @Override public void onBrainWavesChange(Set<BrainWave> brainWaves) {
        handleBrainWavesChange(brainWaves);
      }
    });
  }

  private void handleStateChange(final State state) {
    if (neuroSky != null && state.equals(State.CONNECTED)) {
      neuroSky.start();
    }

    tvState.setText(state.toString());
    Log.d(LOG_TAG, state.toString());
  }

  //O Código comentado trata de TextViews presentess no app originalmetne
  private void handleSignalChange(final Signal signal) {
    switch (signal) {
      case ATTENTION:
        //tvAttention.setText(getFormattedMessage("attention: %d", signal));
        break;
      case MEDITATION:
        //tvMeditation.setText(getFormattedMessage("meditation: %d", signal));
        break;
      case BLINK:
        //tvBlink.setText(getFormattedMessage("blink: %d", signal));
        break;
    }


    Log.d(LOG_TAG, String.format("%s: %d", signal.toString(), signal.getValue()));
  }

  private String getFormattedMessage(String messageFormat, Signal signal) {
    return String.format(Locale.getDefault(), messageFormat, signal.getValue());
  }

  private void handleBrainWavesChange(final Set<BrainWave> brainWaves) {
      if(graphicsAreReady){
          for (BrainWave brainWave : brainWaves) {

            //tentativas de colocar os pontos nos gráficos da "ShowResultsActivity"
              if(brainWave.toString().equals("HIGH_ALPHA")) ShowResultsActivity.grafico1.addNumero((float)brainWave.getValue());
              else if(brainWave.toString().equals("THETA")) ShowResultsActivity.grafico2.addNumero((float)brainWave.getValue());
              else ShowResultsActivity.grafico3.addNumero(brainWave.getValue());
              //Log.d(LOG_TAG, String.format("%s: %d", brainWave.toString(), brainWave.getValue()));
          }
      }

  }


  @OnClick(R.id.btn_connect) void connect() {
    try {
      neuroSky.connect();
      Button b = (Button) findViewById(R.id.btn_disconnect);
      b.setEnabled(true);
    } catch (BluetoothNotEnabledException e) {
      Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
      Log.d(LOG_TAG, e.getMessage());
    }
  }

  @OnClick(R.id.btn_disconnect) void disconnect() { neuroSky.disconnect(); }

  @OnClick(R.id.btn_get_credentials) void goToCredential() {
    if(neuroSky != null && neuroSky.isConnected()){
      neuroSky.start();
      Intent intent = new Intent(this, GetCredentialsActivity.class);
      startActivity(intent);
    } else{
      Toast.makeText(this, "Conecte o dispositivo para prosseguir", Toast.LENGTH_SHORT).show();
    }
  }

  private class SalvaDadosWebService extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... urls) {
      try {
        Log.i("URL", WebServiceUtil.getContentAsString(urls[0]));
        return WebServiceUtil.getContentAsString(urls[0]);

      } catch (IOException e) {
        Log.e("Exception", e.toString());//Observe que aqui uso o log.e e não log.d
        return "Problema ao montar a requisição";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      try {
        Log.d("SALVANDO_SERVIDOR", result);
      } catch (Exception e){
        Log.e("Erro",e.getMessage());
      }
    }


  }
}
