package br.com.andreiabos.braineeg.app;

import android.graphics.Color;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

/**
    A Classe RealTimeGraph serve para criar gráficos dinâmicos com maior facilidade
 */
class RealTimeGraph {
    /**
     * Inteiro para guardar a cor da linha que liga os pontos no gráfico
     */
    private int color;

    /**
     * O gráfico
     */
    private LineChart grafico;

    /**
     * Configura atributos do gráfico.
     *
     * @param viewGrafico a view do gráfico. ex: findViewById(R.id.oGrafico).
     */
    RealTimeGraph (LineChart viewGrafico){
        color = Color.MAGENTA;
        grafico = viewGrafico;

        grafico.getDescription().setEnabled(false);
        //graficoUm.getDescription().setText("Este é o grafico Um");

        grafico.setTouchEnabled(false);
        grafico.setDragEnabled(false);
        grafico.setScaleEnabled(false);
        grafico.setDrawGridBackground(false);
        grafico.setPinchZoom(false);

        grafico.setBackgroundColor(Color.WHITE);

        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        grafico.setData(data);

        Legend l = grafico.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.WHITE);

        XAxis xl = grafico.getXAxis();
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(true);
        xl.setAvoidFirstLastClipping(true);
        xl.setEnabled(true);

        YAxis leftAxis = grafico.getAxisLeft();
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setDrawGridLines(true);
        leftAxis.setAxisMaximum(10f);
        leftAxis.setAxisMinimum(-10f);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = grafico.getAxisRight();
        rightAxis.setEnabled(false);

        grafico.getAxisLeft().setDrawGridLines(true);
        grafico.getXAxis().setDrawGridLines(false);
        grafico.setDrawBorders(false);
    }

    /**
     * Define a cor da linha que liga os pontos do gráfico.
     *
     * @param cor o id da cor. ex: Color.AZUL
     */
    void setColor(int cor){
        color = cor;
    }

    /**
     * Configura atributos do gráfico.
     *
     * @return LineDataSet configurado.
     */
    private LineDataSet createSet(){
        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setLineWidth(3f);
        set.setColor(color);
        set.setHighlightEnabled(false);
        set.setDrawValues(false);
        set.setDrawCircles(true);
        set.setCircleColor(Color.GRAY);
        set.setDrawCircleHole(false);
        //set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        //set.setCubicIntensity(0.2f);
        return set;
    }

    /**
     * Adiciona o ponto especificado ao gráfico.
     *
     * @param y ponto a ser adicionado.
     */
    void addNumero(float y){
        LineData data = grafico.getData();

        if(data != null){
            ILineDataSet set = data.getDataSetByIndex(0);

            if(set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            float x = (float) set.getEntryCount();

            data.addEntry(new Entry(x, y), 0);
            data.notifyDataChanged();

            grafico.notifyDataSetChanged();

            grafico.setVisibleXRangeMaximum(20f);
            grafico.setMaxVisibleValueCount(10);
            grafico.moveViewToX(set.getEntryCount());
        }
    }
}
