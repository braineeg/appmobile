package br.com.andreiabos.braineeg.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;

/**
 * Tela para pegar credenciais do aluno
 *
 * TODO Tela para pegar credenciais do aluno.
 */
public class GetCredentialsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_credentials);

        ButterKnife.bind(this);

        Intent intent = new Intent(this, ShowResultsActivity.class);
        startActivity(intent);
    }
}
