package br.com.andreiabos.braineeg.app;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import butterknife.ButterKnife;

/**
 * Tela que mostra os gráficos com pontos adicionáveis.
 *
 * TODO Receber as leituras do sensor e adicionar aos gráficos
 */
public class ShowResultsActivity extends AppCompatActivity {

    public static RealTimeGraph grafico1;
    public static RealTimeGraph grafico2;
    public static RealTimeGraph grafico3;
    public static RealTimeGraph grafico4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_results);
        ButterKnife.bind(this);

        grafico1 = new RealTimeGraph(findViewById(R.id.grafico1));
        grafico1.setColor(Color.MAGENTA);
        grafico2 = new RealTimeGraph(findViewById(R.id.grafico2));
        grafico2.setColor(Color.RED);
        grafico3 = new RealTimeGraph(findViewById(R.id.grafico3));
        grafico3.setColor(Color.BLUE);
        grafico4 = new RealTimeGraph(findViewById(R.id.grafico4));
        grafico4.setColor(Color.GREEN);

        MainActivity.graphicsAreReady = true;
    }
}
